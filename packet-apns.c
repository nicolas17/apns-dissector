// SPDX-FileCopyrightText: 2021 Nicolás Alvarez <nicolas.alvarez@gmail.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <ws_symbol_export.h>
#include <ws_version.h>
#include <epan/packet.h>
#include <epan/dissectors/packet-tcp.h>
#include <epan/dissectors/packet-tls.h>

#define APNS_TLS_PORT 5223

WS_DLL_PUBLIC_DEF const gchar plugin_version[] = PLUGIN_VERSION;
WS_DLL_PUBLIC_DEF const int plugin_want_major = WIRESHARK_VERSION_MAJOR;
WS_DLL_PUBLIC_DEF const int plugin_want_minor = WIRESHARK_VERSION_MINOR;

WS_DLL_PUBLIC void plugin_register(void);

static int proto_apns = -1;
static int hf_apns_command = -1;
static int hf_apns_length = -1;
static int hf_apns_field_type = -1;
static int hf_apns_field_length = -1;
static int hf_apns_field_payload = -1;

static int ett_apns = -1;
static int ett_field = -1;

struct ItemInfo {
    int id;
    const char* name;
    const int* hf;
};
struct CommandInfo {
    int id;
    const struct ItemInfo* item_infos;
};

// Generated hf_apns_* declarations, APNS_* macros, and item_info_* arrays
#include "apns-decls.h"

static const value_string interface_string[] = {
    { 0x0, "WWAN/Cellular" },
    { 0x1, "Wi-Fi" },
    { 0, NULL }
};
static const value_string active_state_string[] = {
    { 0x1, "Active" },
    { 0x2, "Inactive" },
    { 0, NULL }
};
// this looks like a bitfield but apparently only one value is possible,
// they're never OR'd
static const value_string push_type_string[] = {
    { 0x0, "None" },
    { 0x1, "Complication" },
    { 0x2, "VoIP" },
    { 0x4, "Background" },
    { 0x8, "Alert" },
    { 0x10, "FileProvider" },
    { 0x20, "MDM" },
    { 0x40, "Widget" },
    { 0, NULL }
};

static const struct CommandInfo* lookup_command(int command_id) {
    const struct CommandInfo* c = commands;
    while (c->item_infos) {
        if (c->id == command_id) {
            return c;
        }
        ++c;
    }
    return NULL;
}
static const struct ItemInfo* lookup_item(const struct CommandInfo* command, int item_id) {
    const struct ItemInfo* i = command->item_infos;
    while (i->name) {
        if (i->id == item_id) {
            return i;
        }
        ++i;
    }
    return NULL;
}

static int dissect_apns_message(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree _U_, void *data _U_)
{
    guint offset = 0;
    guint command_id = tvb_get_guint8(tvb, offset);

    col_set_str(pinfo->cinfo, COL_PROTOCOL, "APNS");
    /* Clear the info column */
    col_clear(pinfo->cinfo, COL_INFO);

    const struct CommandInfo* command_info = lookup_command(command_id);

    col_add_fstr(pinfo->cinfo, COL_INFO, "Command: %s", val_to_str(command_id, command_names, "Unknown (0x%02x)"));

    proto_item *ti = proto_tree_add_item(tree, proto_apns, tvb, 0, -1, ENC_NA);
    proto_tree *apns_tree = proto_item_add_subtree(ti, ett_apns);

    proto_item_append_text(ti, ", Command: %s", val_to_str(command_id, command_names, "Unknown (0x%02x)"));

    proto_tree_add_item(apns_tree, hf_apns_command, tvb, offset, 1, ENC_BIG_ENDIAN);
    offset += 1;
    proto_tree_add_item(apns_tree, hf_apns_length, tvb, offset, 4, ENC_BIG_ENDIAN);
    offset += 4;
    while (tvb_reported_length_remaining(tvb, offset) >= 3) {
        guint field_type = tvb_get_guint8(tvb, offset);
        guint field_length = tvb_get_ntohs(tvb, offset+1);

        const struct ItemInfo* item_info = command_info ? lookup_item(command_info, field_type) : NULL;

        proto_tree *field_tree;
        field_tree = proto_tree_add_subtree_format(apns_tree, tvb, offset, field_length+3, ett_field, NULL,
                "Field: %s (0x%x)", (item_info ? item_info->name : "Unknown"), field_type);

        proto_tree_add_item(field_tree, hf_apns_field_type, tvb, offset, 1, ENC_BIG_ENDIAN);
        offset += 1;
        proto_tree_add_item(field_tree, hf_apns_field_length, tvb, offset, 2, ENC_BIG_ENDIAN);
        offset += 2;
        int field_hf = hf_apns_field_payload;
        if (item_info) {
            field_hf = *item_info->hf;
        }
        int encoding = ENC_BIG_ENDIAN;
        if (field_hf == hf_connect_tls_time ||
            field_hf == hf_connected_server_time ||
            field_hf == hf_connected_unknown_ts
        ) {
            encoding = ENC_BIG_ENDIAN|ENC_TIME_MSECS;
        }
        proto_item* value_item = proto_tree_add_item(field_tree, field_hf, tvb, offset, field_length, encoding);
        offset += field_length;

        // show the value in the root tree element for the field too
        char* formatted_value = proto_item_get_display_repr(wmem_packet_scope(), value_item);
        proto_item_append_text(field_tree, ": %s", formatted_value);
    }

    return tvb_captured_length(tvb);
}
static guint get_apns_message_len(packet_info *pinfo _U_, tvbuff_t *tvb, int offset, void *data _U_)
{
    return (guint)tvb_get_ntohl(tvb, offset+1) + 5;
}

static int dissect_apns(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree _U_, void *data _U_)
{
    tcp_dissect_pdus(tvb, pinfo, tree, TRUE, 5, get_apns_message_len, dissect_apns_message, data);
    return tvb_captured_length(tvb);
}

void proto_register_apns(void)
{
    static hf_register_info hf[] = {
        { &hf_apns_command,                 { "Command",            "apns.command",                     FT_UINT8,   BASE_DEC,  VALS(command_names), 0x0, NULL, HFILL } },
        { &hf_apns_length,                  { "Payload length",     "apns.length",                      FT_UINT32,  BASE_DEC,  NULL, 0x0, NULL, HFILL } },
        { &hf_apns_field_type,              { "Type",               "apns.field.type",                  FT_UINT8,   BASE_HEX,  NULL, 0x0, NULL, HFILL } },
        { &hf_apns_field_length,            { "Length",             "apns.field.length",                FT_UINT16,  BASE_DEC,  NULL, 0x0, NULL, HFILL } },
        { &hf_apns_field_payload,           { "Value",              "apns.field.payload",               FT_BYTES,   BASE_NONE, NULL, 0x0, NULL, HFILL } },

#include "apns-fields.h"
    };
    static gint *ett[] = {
        &ett_apns,
        &ett_field
    };
    proto_apns = proto_register_protocol (
        "Apple Push Notification Service", /* name */
        "APNS",          /* short_name  */
        "apns"           /* filter_name */
    );
    proto_register_field_array(proto_apns, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));
}

void proto_reg_handoff_apns(void)
{
    static dissector_handle_t apns_handle;

    apns_handle = register_dissector("apns", dissect_apns, proto_apns);
    dissector_add_string("tls.alpn", "apns-security-v3", apns_handle);
    ssl_dissector_add(APNS_TLS_PORT, apns_handle);
}

void plugin_register(void)
{
    static proto_plugin plug_apns;

    plug_apns.register_protoinfo = proto_register_apns;
    plug_apns.register_handoff = proto_reg_handoff_apns;
    proto_register_plugin(&plug_apns);
}
