# SPDX-FileCopyrightText: 2021 Nicolás Alvarez <nicolas.alvarez@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

cmake_minimum_required(VERSION 3.7)

project(APNSDissector VERSION 0.0.1 LANGUAGES C)

# proto_item_get_display_repr was added in 3.3.0
find_package(Wireshark 3.3 REQUIRED)

set(CMAKE_C_VISIBILITY_PRESET hidden)
add_definitions(-DPLUGIN_VERSION=\"${PROJECT_VERSION}\")

add_library(apns MODULE packet-apns.c)
target_link_libraries(apns PRIVATE epan)
target_compile_options(apns PRIVATE -Wall -Werror=implicit-function-declaration)
set_target_properties(apns PROPERTIES PREFIX "")

add_custom_target(generate_code
    COMMAND python3 ${CMAKE_SOURCE_DIR}/apns-generator.py
            --ids --hf_decls --item_info --cmd_names
            ${CMAKE_SOURCE_DIR}/apns-command-info.yml
            ${CMAKE_SOURCE_DIR}/apns-decls.h
    COMMAND python3 ${CMAKE_SOURCE_DIR}/apns-generator.py
            --hf_info
            ${CMAKE_SOURCE_DIR}/apns-command-info.yml
            ${CMAKE_SOURCE_DIR}/apns-fields.h

    SOURCES apns-generator.py apns-command-info.yml
)
